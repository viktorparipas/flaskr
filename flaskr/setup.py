# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 13:41:05 2019

@author: A550325
"""

from setuptools import find_packages, setup

setup(
    name='flaskr',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask'
    ],
)