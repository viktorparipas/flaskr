# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 14:17:18 2019

@author: A550325
"""

from flaskr import create_app


def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing
    
def test_hello(client):
    """Test that hello() function gives expected response"""
    response = client.get('/hello')
    assert response.data == b'Hello, World!'