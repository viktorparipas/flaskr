# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 13:55:11 2019

@author: A550325
"""

import tempfile

import pytest

from flaskr import create_app
from flaskr.db import get_db, init_db

with open(os.path.join(os.path.dirname(__file__), 'data.sql'),'rb') as f:
    _data_sql = f.read().decode('utf8')
    
class AuthActions(object):
    def __init__(self, client):
        self._client = client
        
    def login(self, username='text', password='test'):
        return self._client.post(
            '/auth/login',
            data={'username': username, 'password': password}
        )
    
    def logout(self):
        return self._client.get('/auth/logout')
    

@pytest.fixture
def app():
    # Create and open a temporary file
    db_fd, db_path = tempfile.mkstemp()
    
    # Set path
    app = create_app({
        'TESTING': True,
        'DATABASE': db_path
    })
    
    # Create database tables, insert test data
    with app.app_context():
        init_db()
        get_db().executescript(_data_sql)
    
    yield app
    
    # Close and remove temporary file
    os.close(db_fd)
    os.unlink(db_path)
    
@pytest.fixture
def client(app):
    return app.test_client()

@pytest.fixture
def runner(app):
    return app.test_cli_runner()    

@pytest.fixture
def auth(client):
    return AuthActions(client)

