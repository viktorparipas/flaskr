# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 16:14:18 2019

@author: A550325
"""

# Importing modules
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

# Creating blueprint
bp = Blueprint('blog', __name__)

# Index view
@bp.route('/')
def index():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    
    return render_template('blog/index.html', posts=posts)

# Create view
@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    """In this view the user can create a post with a title and a body, and
    commit it to the database"""
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None
        
        if not title:
            error = "Title is required."
            
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (?, ?, ?)',
                (title, body, g.user['id'])
            )
            db.commit()
            return redirect(url_for('blog.index'))
        
    return render_template('blog/create.html')


def get_post(id, check_author=True):
    """This function returns the post with the given id from the database."""
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ? ',
        (id,)
    ).fetchone()
    
    if post is None:
        abort(404, f"Post id {id} does not exist.")
        
    if check_author and post['author_id'] != g.user['id']:
        abort(403)
        
    return post
            

# Update view
@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    """In this view the user can modify the post with the given id"""
    post = get_post(id)
    
    if request.method == "POST":
        title = request.form['title']
        body = request.form['body']
        error = None
        
        if not title:
            error = "Title is required."
            
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))
        
    return render_template('blog/update.html', post=post)      

# Delete view
@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id) # aborts if no post with given id
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))